package me.icocoa.hello

/**
 * @author lingchax
 */
fun sum(a: Int, b: Int): Int {
    return a + b
}

fun printSum(a: Int, b: Int): Unit {
    print(a + b)
}

fun main(args: Array<String>) {
    println("Hello World!")
}
